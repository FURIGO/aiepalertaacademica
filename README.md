# Projeto AIEP

> Desenvolvimento de protótipos de página para o projeto AIEP

---

**Backlog**

  1. Início _[Dezembro 18, 2018]_
     - Criação local do projeto
     - Definição da estrutura do projeto
     - Adição dos arquivos iniciais

  2. Login _[Dezembro 18, 2018]_
     - Definição da página de login
     - Adição dos arquivos CSS predefinidos (`main.css` e `theme-aiep.min.css`)
     - **Impedimento**
        - Ausência de imagens (logo, background, etc)
        - CSS predifinido não responde à classes padrão do Bootstrap

  3. Login _[Dezembro 19, 2018]_
     - Finalização da marcação HTML
     - Definição das classes CSS
     - **Impedimento**
        - Ausência de imagens (logo, background, etc)
        - Falta de login no GitLab

  4. Login _[Dezembro 20, 2018]_
     - Entrega do layout do login no repositório

  5. Sequência de telas RF009, RF006 e RF007 _[Janeiro 07, 2019]_
     - Marcação HTML da _RF009: Configurar usuarios que tendrán acceso al sistema de alertas_
     - **Pontos de Atenção**
        - As alterações feitas no CSS do login ainda não estão commitadas no repositório
        - Entre os arquivos de imagem recebidos faltam ícones como _user_ e _close_
        - As imagens de protótipos contidas no documento não são adequadas para desenvolvimento. O ideal seria utilizar os arquivos originais (.PSD, .PNG, etc) em tamanho real
        - Não existe protótipos responsivos
